//
//  ViewController.swift
//  BasicTypes
//
//  Created by Aleksey Knysh on 1/13/21.
//  Copyright © 2021 Tsimafei Harhun. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        let studet = Student(name: "Vasia", surname: "Kuralesov", gender: "man", yearOfBirth: 1986, averageScore: 3.2, height: 176.5, attendigClasses: true, knowsHowToJoke: false)
        let student1 = Student(name: "Ulianna", surname: "Leopardovna", gender: "woman", yearOfBirth: 2005, averageScore: 9.9, height: 200.1, attendigClasses: false, knowsHowToJoke: nil)
        let student2 = Student(name: "Ilya", surname: "Developer", gender: "man", yearOfBirth: 1994, averageScore: 8.5, height: 193.3, attendigClasses: true, knowsHowToJoke: false)
        let student3 = Student(name: "Zoya", surname: "Nepobedimya", gender: "woman", yearOfBirth: 1945, averageScore: 10.01, height: 195.5, attendigClasses: true, knowsHowToJoke: true)
        let student4 = Student(name: "Dima", surname: "Kushner", gender: "man", yearOfBirth: 2003, averageScore: 8.3, height: 171.5, attendigClasses: false, knowsHowToJoke: nil)
        
        let groupStudents = [studet, student1, student2, student3, student4]
        
        for pupil in groupStudents {
            print("\nИмя - \(pupil.name)\nФамилия - \(pupil.surname)\nПол - \(pupil.gender)\nДень рождения - \(pupil.yearOfBirth)\nСредний бал - \(pupil.averageScore)\nРост - \(pupil.height)\nПрисутствие на занятиях - \(pupil.attendigClasses)")
            
            print("\nВыводим имя студента - \(pupil.gender == "man" ? "Mr" : "Miss") \(pupil.name)\n")
            
            let joker = pupil.knowsHowToJoke
            if joker != nil {
                print("Чуство юмора - \(pupil.name) петросян\n")
            } else {
                print("Чуство юмора - \(pupil.name) такой себе шутник\n")
            }
            
            let old = 2020 - pupil.yearOfBirth
            if old < 17 {
                print("\(pupil.name) - Ты ещё школьник\n\n")
            } else if old == 17 {
                print("\(pupil.name) - Ты первокурсник\n\n")
            } else if old >= 18 {
                print("\(pupil.name) - Ты уже не студент\n\n")
            }
        }
    }
}
