//
//  Student.swift
//  BasicTypes
//
//  Created by Aleksey Knysh on 1/13/21.
//  Copyright © 2021 Tsimafei Harhun. All rights reserved.
//

import Foundation

struct Student {
    let name: String
    let surname: String
    var gender: String
    var yearOfBirth: Int
    var averageScore: Double
    var height: Float
    var attendigClasses: Bool
    var knowsHowToJoke: Bool?
}
